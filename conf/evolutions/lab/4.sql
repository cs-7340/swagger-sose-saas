-- MySQL dump 10.13  Distrib 8.0.21, for macos10.15 (x86_64)
--
-- Host: localhost    Database: lab
-- ------------------------------------------------------
-- Server version	8.0.23
--
-- Table structure for table `publications`
--

# --- !Ups
CREATE TABLE `abstracts` (
   `id` int NOT NULL AUTO_INCREMENT,
   `abstract_str` TEXT DEFAULT NULL,
   `ee` varchar(250) DEFAULT NULL,
   `title` varchar(250) DEFAULT NULL,
   PRIMARY KEY (`id`)
);

# --- !Downs
DROP TABLE IF EXISTS `abstracts`;