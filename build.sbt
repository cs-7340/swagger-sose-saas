import play.sbt.PlayImport.cache.exclude

name := """ebean-backend"""

version := "1.0-SNAPSHOT"

lazy val `root` = (project in file(".")).enablePlugins(PlayJava, PlayEbean)
routesGenerator := InjectedRoutesGenerator
scalaVersion := "2.11.8"
resolvers ++= Seq(
  "Typesafe" at "http://repo.typesafe.com/typesafe/releases/",
  "Java.net Maven2 Repository" at "http://download.java.net/maven/2/"
)
//libraryDependencies += guice
libraryDependencies += evolutions
libraryDependencies += jdbc
libraryDependencies ++= Seq(
  javaWs
)
libraryDependencies ++= Seq(
  javaJdbc
)
libraryDependencies += ws
libraryDependencies += "javax.json" % "javax.json-api" % "1.1.4"
libraryDependencies += "mysql" % "mysql-connector-java" % "8.0.11"
libraryDependencies ++= Seq(javaWs , specs2 % Test )
libraryDependencies += "cc.mallet" % "mallet" % "2.0.8"
libraryDependencies += "org.json" % "json" % "20210307"
libraryDependencies += "cn.apiclub.third" % "jgap" % "3.6.2"
libraryDependencies += "io.swagger" %% "swagger-play2" % "1.5.3"
libraryDependencies += "org.webjars" %% "webjars-play" % "2.5.0-4"
libraryDependencies += "org.webjars" % "swagger-ui" % "2.2.0"