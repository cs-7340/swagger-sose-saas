package utility;

public enum PublType {
    PROCEEDINGS("proceedings"),
    INPROCEEDINGS("inproceedings"),
    ARTICLE("article");

    public final String type;

    private PublType(String type) {
        this.type = type;
    }
}
