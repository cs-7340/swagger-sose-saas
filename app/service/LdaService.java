package service;

import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Abstracts;
import models.Publication;
import models.PublicationCategories;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import play.libs.Json;

import javax.inject.Inject;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class LdaService {
    Logger LOG = LoggerFactory.getLogger(LdaService.class);
    private final static ObjectMapper MAPPER = new ObjectMapper();
    private final static String REQUEST_URL = "https://apiv2.aminer.cn/n?a=SEARCH__searchapi.SearchPubsCommon___";
    private final static String QUERY_TOKEN = "<Q>";
    private static String REQUEST_QUERY = "{\"action\":\"searchapi.SearchPubsCommon\",\"parameters\":{\"offset\":0,\"size\":1,\"query\":\"<Q>\"},\"schema\":{\"publication\":[\"abstract\"]}}";
    private int numberOfIterations;
    private int numberOfTopics;
    private double alphaSum;
    private double beta;
    private ParallelTopicModel savedModel;
    private InstanceList savedInstanceList;
    private final PaperService paperService;

    @Inject
    public LdaService(final PaperService paperService) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        numberOfTopics = 20;
        numberOfIterations = 2000;
        alphaSum = 50.0/2000;
        beta = 0.01;
        LOG.info("Initializing numberOfTopics:{}, numberOfIterations:{}, alphaSum:{}, beta:{}",
                numberOfTopics, numberOfIterations,alphaSum,beta);
        populateSavedModelAndInstances();
        this.paperService = paperService;
    }

    public void setNumberOfTopics(int numberOfTopics){
        LOG.info("Setting numberOfTopics to new value: {}", numberOfTopics);
        this.numberOfTopics = numberOfTopics;
    }
    public void setNumberOfIterations(int numberOfIterations){
        LOG.info("Setting numberOfIterations to new value: {}", numberOfIterations);
        this.numberOfIterations = numberOfIterations;
    }
    public void setAlphaSum(double alphaSum){
        LOG.info("Setting alphaSum to new value: {}", alphaSum);
        this.alphaSum = alphaSum;
    }
    public void setBeta(double beta){
        LOG.info("Setting beta to new value: {}", beta);
        this.beta = beta;
    }

    public List<String> getPublicationsByTopic(String category){
        List<String> pubTitles = PublicationCategories.getPublicationTitlesByCategory(category);
        List<String> titles = Publication.findByNames(pubTitles).stream().map(Publication::getTitle).distinct().collect(Collectors.toList());
        Collections.sort(titles);
        return titles;
    }

    public List<String> getCategories() throws IOException {
        createLDA();
        return PublicationCategories.findCategoriesAndCount();
    }

    public void trainModel(boolean retrain) throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        Instant begin = Instant.now();
        if(retrain){
            deleteExistingModel();
        }
        storeDataIntoDB();
        LOG.info("Training model");
        System.out.println("Training model....");
        System.out.println("Please be patient it might take up to 5min...");
        createLDA();
        System.out.println("Completed training model");
        LOG.info("Completed training model. Took {}ms",(Instant.now().toEpochMilli() - begin.toEpochMilli()));
    }

    public void deleteExistingModel(){
        Instant begin = Instant.now();
        LOG.info("Deleting model");
        //clear file
        File instanceFile = new File("lda/instances");
        if (instanceFile.delete()) {
            savedInstanceList = null;
            LOG.info("Deleted the instance list file: {}",instanceFile.getName());
        } else {
            LOG.error("Failed to delete instance list file: {}",instanceFile.getName());
        }
        File modelFile = new File("lda/model");
        if (modelFile.delete()) {
            savedModel = null;
            LOG.info("Deleted the model file: {}",modelFile.getName());
        } else {
            LOG.error("Failed to delete model file: {}",modelFile.getName());
        }
        LOG.info("Completed deleting model. Took {}ms",(Instant.now().toEpochMilli() - begin.toEpochMilli()));
    }

    public void storeDataIntoDB() throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        System.out.println("Getting abstracts from api. Please be patient it may take up to 5min...");
        Instant begin = Instant.now();
        URL url = new URL(REQUEST_URL);
        List<Abstracts> abstracts = new ArrayList<>();
        List<String> titles = Publication.getAllPublicationTitles();
        getDataFromExternal(abstracts,titles,url);
        System.out.println("Storing abstracts in database...");
        Abstracts.insertAbstracts(abstracts);
        LOG.info("Completed storing abstracts in database. Took {}ms",(Instant.now().toEpochMilli() - begin.toEpochMilli()));
    }

    private void getDataFromExternal(List<Abstracts> abstracts,List<String> titles, URL url) throws IOException {
        if(titles.size() < 250){
            System.out.println("calling api - "+titles.size());
            List<Abstracts> sub = new ArrayList<>();
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            String jsonInputString = "[";
            for (int i =0;i<titles.size();i++) {
                jsonInputString += REQUEST_QUERY.replace(QUERY_TOKEN, titles.get(i).replaceAll("\"", "\\\\\""));
                if(i != titles.size()-1){
                    jsonInputString +=",";
                }
            }
            jsonInputString += "]";
            jsonInputString = jsonInputString.replace("\n", "").replace("\r", "");

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                JSONObject jsonObject = new JSONObject(response.toString());
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for(int i=0;i<jsonArray.length();i++){
                    JSONArray itemArray = jsonArray.getJSONObject(i).getJSONArray("items");
                    JSONObject item = itemArray.getJSONObject(0);
                    String abs = item.optString("abstract");
                    if (!StringUtils.isEmpty(abs)) {
                        sub.add(new Abstracts(titles.get(i), abs));
                    }
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
            abstracts.addAll(sub);
        }else{
            getDataFromExternal(abstracts,titles.subList(0,titles.size()/2),url);
            getDataFromExternal(abstracts,titles.subList(titles.size()/2,titles.size()),url);
        }

    }

    private void saveModels(List<Abstracts> allAbstracts) throws IOException {
        System.out.println("Retrieved abstracts...");
        if(ObjectUtils.isEmpty(this.savedInstanceList)){
            Iterator<Abstracts> abstractIterator = allAbstracts.iterator();

            System.out.println("Generating InstanceList...");
            ArrayList<Pipe> pipeList = new ArrayList<>();

            pipeList.add(new CharSequenceLowercase());
            pipeList.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
            pipeList.add(new TokenSequenceRemoveStopwords(new File("en.txt"), "UTF-8", false,false,false));
            pipeList.add(new TokenSequence2FeatureSequence());

            InstanceList instances = new InstanceList(new SerialPipes(pipeList));

            while(abstractIterator.hasNext()){
                Abstracts abstracts = abstractIterator.next();
                String pubContent = abstracts.title+" "+abstracts.abstractStr;
                Reader pubReader = new StringReader(pubContent);
                instances.addThruPipe(new CsvIterator(pubReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),
                        3, 2, 1));
            }
            instances.save(new File("instances"));
            savedInstanceList = instances;
        } // data, label, name fields
        if(ObjectUtils.isEmpty(savedModel)){
            System.out.println("Generating TopicModel...");
            // Create a model with 100 topics, alpha_t = 0.01, beta_w = 0.01
            //  Note that the first parameter is passed as the sum over topics, while
            //  the second is the parameter for a single dimension of the Dirichlet prior.
            ParallelTopicModel model = new ParallelTopicModel(this.numberOfTopics, this.alphaSum, this.beta);

            model.addInstances(savedInstanceList);

            // Use two parallel samplers, which each look at one half the corpus and combine
            //  statistics after every iteration.
            model.setNumThreads(5);

            // Run the model for 50 iterations and stop (this is for testing only,
            //  for real applications, use 1000 to 2000 iterations)
            model.setNumIterations(this.numberOfIterations);
            model.estimate();
            model.write(new File("model"));
            savedModel = model;
        }
    }
    private void createLDA() throws IOException {
        if(!PublicationCategories.isEmpty()){
            return;
        }
        List<Abstracts> allAbstracts = Abstracts.getAllAbstracts();
        saveModels(allAbstracts);

        // The data alphabet maps word IDs to strings
        Alphabet dataAlphabet = savedInstanceList.getDataAlphabet();

        //save publication to topic
        List<PublicationCategories> records = new ArrayList<>();
        allAbstracts.forEach(abs -> {
            FeatureSequence tokens = (FeatureSequence) savedModel.getData().get(abs.id.intValue()-1).instance.getData();
            for(int position = 0; position < tokens.getLength(); position++){
                records.add(new PublicationCategories(abs.title,
                        dataAlphabet.lookupObject(tokens.getIndexAtPosition(position)).toString()));
            }
        });
        PublicationCategories.insertAll(records);
    }

    private void populateSavedModelAndInstances() throws IOException {
        try {
            File file = new File("instances");
            if(file.exists()){
                System.out.println("Found saved instances");
                savedInstanceList = InstanceList.load(new File("instances"));
            }
        } catch (Exception e) {
            System.out.println("Failed to load existing instance list");
            savedInstanceList = null;
        }
        try {
            File file = new File("model");
            if(file.exists()){
                System.out.println("Found saved model");
                savedModel = ParallelTopicModel.read(new File("model"));
            }
        } catch (Exception e) {
            System.out.println("Failed to load existing model");
            savedModel = null;
        }
    }
}
