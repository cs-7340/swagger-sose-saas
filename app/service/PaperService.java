package service;

import io.netty.util.internal.StringUtil;
import models.Abstracts;
import models.Publication;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

public class PaperService {

    @Inject
    public PaperService(){

    }

    public String getAbstract(String paperName){
        return Abstracts.getAbstractByTitle(paperName);
    }

    public List<Publication> getPublicationMetadata(String paperName){
        return Publication.findByName(paperName);
    }

    public List<Publication> getPapersPublishedMetadata(String name, String year, String issue){
        return Publication.findByJournalNameVolumneNumber(name,year,issue);
    }

    public List<String> getArticleTitlesByAuthorAndYear(String researcherName, String year){
        List<String> titles = Publication.findArticlesByAuthorNameYear(researcherName, year)
                .stream().map(Publication::getTitle)
                .collect(Collectors.toList());
        titles.removeIf(title-> StringUtil.isNullOrEmpty(title));
        return titles;
    }

    public List<Publication> getArticlesByAuthorAndYear(String researcherName, String year){
        return Publication.findArticlesByAuthorNameYear(researcherName, year);
    }
}
