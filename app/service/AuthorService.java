package service;

import io.netty.util.internal.StringUtil;
import models.Publication;

import javax.inject.Inject;
import java.util.List;

public class AuthorService {
    @Inject
    public AuthorService(){

    }

    public List<String> getAuthorsWithOverNumberOfPapersInArea(int numberOfPapers){
        List<String> names = Publication.findAuthorsWithOverNumberOfPapersInArea(numberOfPapers);
        names.removeIf(name -> StringUtil.isNullOrEmpty(name));
        return names;
    }

    public List<String> getCoAuthorsOfAuthorsWithOverNumberOfPapersInArea(int numberOfPapers){
        List<String> names = Publication.findCoAuthorsOfAuthorWithOverNumberOfPapersInArea(numberOfPapers);
        names.removeIf(name -> StringUtil.isNullOrEmpty(name));
        return names;
    }
}
