package service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ConferenceService {
    private Map<String,String> icws = new HashMap(){
        {
            put("2003","36.169941, -115.139832");
            put("2004","32.715736,-117.161087");
            put("2005","28.4810968,-81.5092697");
            put("2006","41.8333908,-88.012843");
            put("2007","40.7765228,-112.0609145");
            put("2008","39.9385449,116.1165786");
            put("2009","34.0201597,-118.6926166");
            put("2010","25.7823907,-80.2996711");
            put("2011","38.8935124,-77.1550058");
            put("2012","21.3279755,-157.9395051");
            put("2013","37.3708697,-122.0377648");
            put("2014","61.1042028,-150.563999");
            put("2015","40.6971477,-74.2605608");
            put("2016","37.7576792,-122.5078122");
            put("2017","21.3279755,-157.9395051");
            put("2018","37.7576792,-122.5078122");
            put("2019","45.4626478,9.0373036");
            put("2020","39.9385449,116.1165786");
        }
    };
    private Map<String,String> scc = new HashMap(){
        {
            put("2004","32.715736,-117.161087");
            put("2005","28.4810968,-81.5092697");
            put("2006","41.8333908,-88.012843");
            put("2007","40.7765228,-112.0609145");
            put("2008","21.3279755,-157.9395051");
            put("2009","12.953847,77.3500464");
            put("2010","25.7823907,-80.2996711");
            put("2011","38.8935124,-77.1550058");
            put("2012","21.3279755,-157.9395051");
            put("2013","37.3708697,-122.0377648");
            put("2014","61.1042028,-150.563999");
            put("2015","40.6971477,-74.2605608");
            put("2016","37.7576792,-122.5078122");
            put("2017","21.3279755,-157.9395051");
            put("2018","37.7576792,-122.5078122");
            put("2019","45.4626478,9.0373036");
            put("2020","39.9385449,116.1165786");
        }
    };


    @Inject
    public ConferenceService(){

    }

    public List<String> getLocations(String conference, String start, String end){
        List<String> locations = new ArrayList<>();
        int startyear = Integer.parseInt(start.trim());
        int stopyear = Integer.parseInt(end.trim());
        if(conference.equalsIgnoreCase("icws")){
            while (startyear <= stopyear){
                locations.add(icws.get(startyear+""));
                startyear+=1;
            }
        }else if(conference.equalsIgnoreCase("scc")){
            while (startyear <= stopyear){
                locations.add(scc.get(startyear+""));
                startyear+=1;
            }
        }
        return locations.stream().distinct().collect(Collectors.toList());
    }
}
