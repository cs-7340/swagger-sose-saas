package service;

import models.QosResponse;
import org.jgap.*;
import org.jgap.audit.EvolutionMonitor;
import org.jgap.impl.DefaultConfiguration;
import org.jgap.impl.IntegerGene;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

public class QosService {
    /**
     * The total number of times we'll let the population evolve.
     */
    private static final int MAX_ALLOWED_EVOLUTIONS = 50;
    private static final String COST = "cost";
    private static final String RELIABILITY = "reliability";
    private static final String TIME = "time";
    private static final String AVAILABILITY = "availability";

    public static final Map<String,Double> paramWeight = new HashMap<String,Double>(){{
        put(COST,0.35);
        put(RELIABILITY,0.10);
        put(TIME,0.20);
        put(AVAILABILITY,0.35);
    }};

    public static final Map<Integer, List<Map<String,Number>>> services = new HashMap<Integer, List<Map<String,Number>>>(){{
        put(1,new ArrayList<Map<String,Number>>(){{
            add(new HashMap<String,Number>(){{
                put(COST,20);
                put(RELIABILITY,0.95);
                put(TIME,2);
                put(AVAILABILITY,0.95);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,30);
                put(RELIABILITY,0.99);
                put(TIME,3);
                put(AVAILABILITY,0.89);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,23);
                put(RELIABILITY,0.98);
                put(TIME,23);
                put(AVAILABILITY,0.98);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,12);
                put(RELIABILITY,0.99);
                put(TIME,1);
                put(AVAILABILITY,0.02);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,25);
                put(RELIABILITY,0.89);
                put(TIME,3);
                put(AVAILABILITY,0.78);
            }});
        }});
        put(2,new ArrayList<Map<String,Number>>(){{
            add(new HashMap<String,Number>(){{
                put(COST,12);
                put(RELIABILITY,0.70);
                put(TIME,3);
                put(AVAILABILITY,0.70);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,15);
                put(RELIABILITY,0.99);
                put(TIME,5);
                put(AVAILABILITY,0.93);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,53);
                put(RELIABILITY,0.96);
                put(TIME,8);
                put(AVAILABILITY,0.96);
            }});
        }});
        put(3,new ArrayList<Map<String,Number>>(){{
            add(new HashMap<String,Number>(){{
                put(COST,11);
                put(RELIABILITY,0.97);
                put(TIME,9);
                put(AVAILABILITY,0.97);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,12);
                put(RELIABILITY,0.89);
                put(TIME,12);
                put(AVAILABILITY,0.89);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,12);
                put(RELIABILITY,0.90);
                put(TIME,1);
                put(AVAILABILITY,0.90);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,15);
                put(RELIABILITY,0.91);
                put(TIME,3);
                put(AVAILABILITY,0.98);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,18);
                put(RELIABILITY,0.56);
                put(TIME,6);
                put(AVAILABILITY,0.56);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,23);
                put(RELIABILITY,0.68);
                put(TIME,2);
                put(AVAILABILITY,0.67);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,22);
                put(RELIABILITY,0.59);
                put(TIME,1);
                put(AVAILABILITY,0.59);
            }});
            add(new HashMap<String,Number>(){{
                put(COST,21);
                put(RELIABILITY,0.92);
                put(TIME,2);
                put(AVAILABILITY,0.89);
            }});
        }});
    }};

    public static EvolutionMonitor m_monitor;

    @Inject
    public QosService(){}

    public QosResponse makeQosEngine()
            throws Exception {
        Configuration conf = new DefaultConfiguration();
        conf.setPreservFittestIndividual(true);
        conf.setKeepPopulationSizeConstant(false);
        FitnessFunction myFunc = new QosFitnessFunction();
        conf.setFitnessFunction(myFunc);
        m_monitor = new EvolutionMonitor();
        conf.setMonitor(m_monitor);

        Gene[] sampleGenes = new Gene[3];
        sampleGenes[0] = new IntegerGene(conf, 1, 5); // SC1
        sampleGenes[1] = new IntegerGene(conf, 1, 3); // SC2
        sampleGenes[2] = new IntegerGene(conf, 1, 8); // SC3// Pennies
        IChromosome sampleChromosome = new Chromosome(conf, sampleGenes);
        conf.setSampleChromosome(sampleChromosome);
        conf.setPopulationSize(20);

        Genotype population = Genotype.randomInitialGenotype(conf);

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < MAX_ALLOWED_EVOLUTIONS; i++) {
            if (!uniqueChromosomes(population.getPopulation())) {
                throw new RuntimeException("Invalid state in generation "+i);
            }
            if(m_monitor != null) {
                population.evolve(m_monitor);
            }
            else {
                population.evolve();
            }
        }
        long endTime = System.currentTimeMillis();

        // Display the best solution we found.
        // -----------------------------------
        IChromosome bestSolutionSoFar = population.getFittestChromosome();

      String selectedServices = "S1" +
              bestSolutionSoFar.getGene(0).getAllele().toString()+
                " , S2" +
                bestSolutionSoFar.getGene(1).getAllele().toString()+
                " , S3" +
                bestSolutionSoFar.getGene(2).getAllele().toString();

        return new QosResponse(endTime - startTime, bestSolutionSoFar.getFitnessValue(), selectedServices);
    }

    /**
     * @param a_pop the population to verify
     * @return true if all chromosomes in the populationa are unique
     *
     * @author Klaus Meffert
     * @since 3.3.1
     */
    public static boolean uniqueChromosomes(Population a_pop) {
        // Check that all chromosomes are unique
        for(int i=0;i<a_pop.size()-1;i++) {
            IChromosome c = a_pop.getChromosome(i);
            for(int j=i+1;j<a_pop.size();j++) {
                IChromosome c2 =a_pop.getChromosome(j);
                if (c == c2) {
                    return false;
                }
            }
        }
        return true;
    }

    private class QosFitnessFunction extends FitnessFunction{
        public QosFitnessFunction(){}
        /**
         * Determine the fitness of the given Chromosome instance. The higher the
         * return value, the more fit the instance. This method should always
         * return the same fitness value for two equivalent Chromosome instances.
         *
         * @param a_subject the Chromosome instance to evaluate
         *
         * @return positive double reflecting the fitness rating of the given
         * Chromosome
         * @since 2.0 (until 1.1: return type int)
         * @author Neil Rotstan, Klaus Meffert, John Serri
         */
        public double evaluate(IChromosome a_subject) {
            double costValue = getCostValue(a_subject);
            double reliabilityValue = getReliabilityValue(a_subject);
            double timeValue = getTimeValue(a_subject);
            double availabilityValue = getAvailabilityValue(a_subject);

            return (costValue*QosService.paramWeight.get(COST))
                    +(reliabilityValue*QosService.paramWeight.get(RELIABILITY))
                    +(timeValue*QosService.paramWeight.get(TIME))
                    +(availabilityValue*QosService.paramWeight.get(AVAILABILITY));
        }

        protected double getCostValue(IChromosome a_subject){
            int sc1Selection = (Integer) a_subject.getGene(0).getAllele();
            int sc2Selection = (Integer) a_subject.getGene(1).getAllele();
            int sc3Selection = (Integer) a_subject.getGene(2).getAllele();
            int totalMaxCost = QosService.services.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().stream().mapToInt(v -> v.get(COST).intValue()).max())).values().stream().mapToInt(OptionalInt::getAsInt).sum();

            return 1 - ((QosService.services.get(1).get(sc1Selection-1).get(COST).intValue()
                    +QosService.services.get(2).get(sc2Selection-1).get(COST).intValue()
                    +QosService.services.get(3).get(sc3Selection-1).get(COST).intValue())/(totalMaxCost*1.0));
        }

        /** P(S1) & P(S2) & P(S3) == P(FullReliability) **/
        protected double getReliabilityValue(IChromosome a_subject){
            int sc1Selection = (Integer) a_subject.getGene(0).getAllele();
            int sc2Selection = (Integer) a_subject.getGene(1).getAllele();
            int sc3Selection = (Integer) a_subject.getGene(2).getAllele();

            return QosService.services.get(1).get(sc1Selection-1).get(RELIABILITY).doubleValue()
                    *QosService.services.get(2).get(sc2Selection-1).get(RELIABILITY).doubleValue()
                    *QosService.services.get(3).get(sc3Selection-1).get(RELIABILITY).doubleValue();
        }

        protected double getTimeValue(IChromosome a_subject){
            int sc1Selection = (Integer) a_subject.getGene(0).getAllele();
            int sc2Selection = (Integer) a_subject.getGene(1).getAllele();
            int sc3Selection = (Integer) a_subject.getGene(2).getAllele();
            int totalMaxTime = QosService.services.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().stream().mapToInt(v -> v.get(TIME).intValue()).max())).values().stream().mapToInt(OptionalInt::getAsInt).sum();

            return 1 - ((QosService.services.get(1).get(sc1Selection-1).get(TIME).intValue()
                    +QosService.services.get(2).get(sc2Selection-1).get(TIME).intValue()
                    +QosService.services.get(3).get(sc3Selection-1).get(TIME).intValue())/(totalMaxTime*1.0));
        }

        /** P(S1) & P(S2) & P(S3) == P(FullAvailability) **/
        protected double getAvailabilityValue(IChromosome a_subject){
            int sc1Selection = (Integer) a_subject.getGene(0).getAllele();
            int sc2Selection = (Integer) a_subject.getGene(1).getAllele();
            int sc3Selection = (Integer) a_subject.getGene(2).getAllele();

            return QosService.services.get(1).get(sc1Selection-1).get(AVAILABILITY).doubleValue()
                    *QosService.services.get(2).get(sc2Selection-1).get(AVAILABILITY).doubleValue()
                    *QosService.services.get(3).get(sc3Selection-1).get(AVAILABILITY).doubleValue();
        }
    }
}
