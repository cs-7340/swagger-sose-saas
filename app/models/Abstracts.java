package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@ApiModel
@Entity
public class Abstracts extends Model {
    @Id
    public Long id;
    public String title;
    public String ee;
    @JsonProperty("abstract")
    public String abstractStr;

    public Abstracts(String title, String abstractStr){
        this.title = title;
        this.abstractStr = abstractStr;
    }

    public static void insertAbstracts(List<Abstracts> abstractsList){
        if(Ebean.getServer("lab").find(Abstracts.class)
                .findList().isEmpty()){
            Ebean.getServer("lab").insertAll(abstractsList);
        }
    }

    public static List<Abstracts> getAllAbstracts(){
        return Ebean.getServer("lab").find(Abstracts.class).where().orderBy("id asc").findList();
    }

    public static String getAbstractByTitle(String title){
        return Ebean.getServer("lab").find(Abstracts.class)
                .where().eq("title",title).findList().get(0).abstractStr;
    }
}
