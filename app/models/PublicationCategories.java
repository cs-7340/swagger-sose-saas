package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import io.swagger.annotations.ApiModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@ApiModel
@Entity
public class PublicationCategories extends Model {
    @Id
    public Long id;
    public String title;
    public String category;

    public PublicationCategories(String title, String category){
        this.title = title;
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public String getTitle() {
        return title;
    }

    public static void insertAll(List<PublicationCategories> publicationCategories){
        Ebean.getServer("lab").insertAll(publicationCategories);
    }

    public static boolean isEmpty(){
        return Ebean.getServer("lab").find(PublicationCategories.class)
                .findList().isEmpty();
    }

    public static List<String> findCategoriesAndCount() {
        List<String> topicCount =  Ebean
                .getServer("lab")
                .createSqlQuery("SELECT COUNT(Id), category\n" +
                        "FROM publication_categories\n" +
                        "GROUP BY category\n"+
                        "ORDER BY COUNT(Id) DESC")
                .findList().stream().map(sqlRow -> sqlRow.get("category")+"="+sqlRow.get("count(id)"))
                .collect(Collectors.toList());
        return topicCount;
    }

    public static List<String> getPublicationTitlesByCategory(String category){
        return Ebean
                .getServer("lab").find(PublicationCategories.class)
                .where()
                .eq("category",category)
                .findList().stream().map(PublicationCategories::getTitle).collect(Collectors.toList());
    }

}
