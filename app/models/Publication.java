package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import utility.PublType;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

@ApiModel
@Entity
public class Publication extends Model{
    @Id
    public Long id;
    public String author;
    public String orcId;
    public String editor;
    public String title;
    public String bookTitle;
    public String pages;
    public String year;
    public String address;
    public String journal;
    public String volume;
    public String number;
    public String month;
    public String url;
    public String ee;
    public String cdrom;
    public String cite;
    public String publisher;
    public String note;
    public String crossref;
    public String isbn;
    public String series;
    public String school;
    public String chapter;
    public String publType;
    public String mdate;
    public String reviewid;
    public String rating;
    public String cdate;
    public String dblp;
    public String pubKey;

    public Long getId(){
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public static void insert(List<Publication> publications){
        Ebean.getServer("lab").insertAll(publications);
    }

    public static List<Publication> getAllPublications(){
        return Ebean.getServer("lab").find(Publication.class).where().orderBy("id asc").findList();
    }

    public static List<String> getAllPublicationTitles(){
        return Ebean.getServer("lab").find(Publication.class).where().eq("publType",PublType.INPROCEEDINGS.type).orderBy("id asc").findList()
                .stream().map(Publication::getTitle)
                .distinct()
                .collect(Collectors.toList());
    }

    public static List<Publication> findByIds(List<Long> publicationIds){
        return Ebean.getServer("lab").find(Publication.class).where().idIn(publicationIds).findList();
    }

    public static List<Publication> findByName(String paperName) {
        List<Publication> pubs = Ebean
                .getServer("lab").find(Publication.class)
                .where()
                .eq("title", paperName)
                .findList();
        List<String> authors = pubs.stream().map(Publication::getAuthor).collect(Collectors.toList());
        String authorsStr = String.join(" ", authors);
        pubs.stream().forEach(pub -> pub.author = authorsStr);
        return Collections.singletonList(pubs.get(0));
    }

    public static List<Publication> findByNames(List<String> paperNames) {
        List<Publication> pubs = Ebean
                .getServer("lab").find(Publication.class)
                .where()
                .in("title",paperNames)
                .findList();
        return pubs;
    }

    public static List<Publication> findByJournalNameVolumneNumber(String name, String year, String issue) {
        List<Publication> pubs = Ebean
                .getServer("lab").find(Publication.class)
                .where()
                .eq("journal", name)
                .eq("volume", year)
                .eq("number", issue)
                .eq("publType", PublType.ARTICLE.type)
                .findList();
        List<Publication> newPubs = new ArrayList<>();
        Map<String, List<Publication>> pubsPerTitle = pubs.stream()
                .collect(groupingBy(Publication::getTitle));

        pubsPerTitle.keySet().stream().forEach(key ->{
            Publication pub = pubsPerTitle.get(key).get(0);
            pub.author = String.join(" ", pubsPerTitle.get(key).stream().map(Publication::getAuthor).collect(Collectors.toList()));
            newPubs.add(pub);
        });
        return newPubs;
    }

    public static List<Publication> findArticlesByAuthorNameYear(String name, String year) {
        return Ebean
                .getServer("lab")
                .find(Publication.class)
                .where()
                .eq("author",name)
                .eq("year",year)
                .eq("publ_type",PublType.ARTICLE.type)
                .findList();
    }

    public static List<String> findAuthorsWithOverNumberOfPapersInArea(int numberOfPapers) {
        List<String> authors =  Ebean
                .getServer("lab")
                .createSqlQuery("SELECT author\n" +
                        "FROM publication\n" +
                        "WHERE publ_type <> ?\n" +
                        "GROUP BY author\n" +
                        "HAVING COUNT(*) > ?;")
                .setParameter(1,PublType.PROCEEDINGS.type)
                .setParameter(2, numberOfPapers)
                .findList().stream().map(sqlRow -> sqlRow.get("author"))
                .map(Object::toString).collect(Collectors.toList());
        Collections.sort(authors);
        return authors;
    }

    public static List<String> findCoAuthorsOfAuthorWithOverNumberOfPapersInArea(int numberOfPapers) {
        List<String> authors = findAuthorsWithOverNumberOfPapersInArea(numberOfPapers);
               List<String> titles = Ebean
                .getServer("lab")
                .find(Publication.class)
                 .where()
                 .in("author",authors)
                 .findList().stream().map(Publication::getTitle).collect(Collectors.toList());

               List<String> coAuthors = Ebean
                       .getServer("lab")
                       .find(Publication.class)
                       .where()
                       .in("title",titles)
                       .findList().stream().map(Publication::getAuthor).collect(Collectors.toList());

                coAuthors.removeIf(coauthor-> authors.contains(coauthor));
                Collections.sort(coAuthors);
                return coAuthors.stream().distinct().collect(Collectors.toList());
    }
}
