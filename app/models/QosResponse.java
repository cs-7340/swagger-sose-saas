package models;

import io.swagger.annotations.ApiModel;

@ApiModel
public class QosResponse {
    private long evolutionTimeInMS;
    private double fitnessValue;
    private String serviceComposition;

    public QosResponse(long evolutionTime, double fitnessValue, String serviceComposition){
        this.evolutionTimeInMS = evolutionTime;
        this.fitnessValue = fitnessValue;
        this.serviceComposition = serviceComposition;
    }

    public long getEvolutionTimeInMS() {
        return evolutionTimeInMS;
    }
    public double getFitnessValue() {
        return fitnessValue;
    }
    public String getServiceComposition() {
        return serviceComposition;
    }
}
