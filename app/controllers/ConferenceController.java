package controllers;

import ch.qos.logback.core.status.ErrorStatus;
import ch.qos.logback.core.status.InfoStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import service.ConferenceService;

import javax.inject.Inject;
import java.util.List;

@Api(value = "Conference Controller", description = "Find Conferences", produces = "application/json")
public class ConferenceController extends Controller {
    @Inject
    ConferenceService conferenceService;

    @ApiOperation(value = "Find Locations given conference name, and year duration", notes = "Get list of comma separated geolocations.", response = List.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result findConferenceYearLocation(String conferenceName, String startYear, String stopYear) {
        try {
            if(!conferenceName.equalsIgnoreCase("icws") && !conferenceName.equalsIgnoreCase("scc")){
                return notFound("We currently only support icws and scc conferences").withHeader(
                        "Access-Control-Allow-Origin","*");
            }
            return ok(Json.toJson(conferenceService.getLocations(conferenceName,startYear,stopYear))).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }

    }
}
