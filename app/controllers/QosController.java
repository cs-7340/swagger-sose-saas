package controllers;

import ch.qos.logback.core.status.ErrorStatus;
import ch.qos.logback.core.status.InfoStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import models.QosResponse;
import org.apache.http.HttpStatus;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.QosService;

import javax.inject.Inject;

@Api(value = "Qos Controller", description = "Find Optimal Service Composition", produces = "application/json")
public class QosController extends Controller {
    @Inject
    QosService qosService;

    @ApiOperation(value = "Find Optimized Service Composition", notes = "Get service composition", response = QosResponse.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result findOptimizedServiceComposition(){
        try {
            return ok(Json.toJson(qosService.makeQosEngine())).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
    }
}
