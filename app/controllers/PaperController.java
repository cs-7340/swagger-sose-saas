package controllers;

import ch.qos.logback.core.status.ErrorStatus;
import ch.qos.logback.core.status.InfoStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import models.Abstracts;
import models.Publication;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import service.PaperService;

import javax.inject.Inject;
import java.util.List;

@Api(value = "Paper Controller", description = "Get MetaData on Papers", produces = "application/json")
public class PaperController extends Controller {
    @Inject
    PaperService paperService;

    @ApiOperation(value = "Get Publications given paper title", notes = "Get list of publications", response = Publication.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result getPublicationMetadata(String paperName) {
        try {
            return ok(Json.toJson(paperService.getPublicationMetadata(paperName))).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }

    }

    @ApiOperation(value = "Get Publications given journal, year, and issue", notes = "Get list of publications", response = Publication.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result getPapersPublishedMetadata(String name, String year, String issue) {
        try {
            return ok(Json.toJson(paperService.getPapersPublishedMetadata(name,year,issue))).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }

    }

    @ApiOperation(value = "Get Article titles given author and year", notes = "Get list of string article titles.", response = List.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result getArticleTitles(String researcherName, String year) {
        try {
          return ok(Json.toJson(paperService.getArticleTitlesByAuthorAndYear(researcherName,year))).withHeader(
                  "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }

    }

    @ApiOperation(value = "Get Articles given author and year", notes = "Get list of publications.", response = Publication.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result getArticles(String researcherName, String year) {
        try {
            return ok(Json.toJson(paperService.getArticlesByAuthorAndYear(researcherName,year))).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }

    }

    @ApiOperation(value = "Get Abstract given title", notes = "Get abstract as string.", response = String.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result getAbstract(String name){
        try {
            return ok(Json.toJson(paperService.getAbstract(name))).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
    }
}
