package controllers;

import ch.qos.logback.core.status.ErrorStatus;
import ch.qos.logback.core.status.InfoStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import service.AuthorService;

import javax.inject.Inject;
import java.util.List;

@Api(value = "Author Controller", description = "Get Author names given certain parameters", produces = "application/json")
public class AuthorController extends Controller {
    @Inject
    AuthorService authorService;

    @ApiOperation(value = "Get Authors with X number of papers in Y area", notes = "Get list of author names.", response = List.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result getAuthorsWithOverNumberOfPapersInArea(String area, int numberOfPapers) {
        try {
            if(!area.equalsIgnoreCase("sose")){
                return notFound("We currently only support queries in SOSE area").withHeader(
                        "Access-Control-Allow-Origin","*");
            }
            return ok(Json.toJson(authorService.getAuthorsWithOverNumberOfPapersInArea(numberOfPapers))).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }

    }

    @ApiOperation(value = "Get CoAuthors of Authors with over X number of papers in Y area", notes = "Get list of co0author names.", response = List.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result getCoAuthorOfAuthorsWithOverNumberOfPapersInArea(String area, int numberOfPapers) {
        try {
            if(!area.equalsIgnoreCase("sose")){
                return notFound("We currently only support queries in SOSE area").withHeader(
                        "Access-Control-Allow-Origin","*");
            }
            return ok(Json.toJson(authorService.getCoAuthorsOfAuthorsWithOverNumberOfPapersInArea(numberOfPapers))).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }

    }
}
