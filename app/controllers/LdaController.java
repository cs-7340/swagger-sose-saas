package controllers;

import ch.qos.logback.core.status.ErrorStatus;
import ch.qos.logback.core.status.InfoStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import service.LdaService;

import javax.inject.Inject;

import java.util.List;

import static play.mvc.Results.internalServerError;
import static play.mvc.Results.ok;

@Api(value = "LDA Controller", description = "Configure LDA Model and Retrieve data form model", produces = "application/json")
public class LdaController extends Controller {
    @Inject
    LdaService ldaService;

    @ApiOperation(value = "Load abstracts into the database", response = Result.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result loadDataIntoDB(){
        try {
            ldaService.storeDataIntoDB();
            return ok().withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
    }

    @ApiOperation(value = "Train LDA model", response = Result.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result trainModel(boolean retrain) {
        try {
            ldaService.trainModel(retrain);
            return ok().withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }

    }

    @ApiOperation(value = "Delete LDA model", response = Result.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result deleteModel() {
        try {
            ldaService.deleteExistingModel();
            return ok().withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }

    }

    @ApiOperation(value = "Get Generated Categories from LDA model", notes = "Get list of string categories.", response = List.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result getCategories() {
        try {
            return ok(Json.toJson(ldaService.getCategories())).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }

    }

    @ApiOperation(value = "Get Publications in LDA Category", notes = "Get list of publication titles.", response = List.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorStatus.class) })
    public Result getPublicationsByCategory(String category){
        try {
            return ok(Json.toJson(ldaService.getPublicationsByTopic(category))).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
        catch (Exception e) {
            System.out.println(e);
            return internalServerError(e.getMessage()).withHeader(
                    "Access-Control-Allow-Origin","*");
        }
    }
}
