-- MySQL dump 10.13  Distrib 8.0.21, for macos10.15 (x86_64)
--
-- Host: localhost    Database: lab
-- ------------------------------------------------------
-- Server version	8.0.23
--
-- Table structure for table `publication_categories`
--

# --- !Ups
CREATE TABLE `publication_categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `category` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

# --- !Downs
DROP TABLE IF EXISTS `publication_categories`;