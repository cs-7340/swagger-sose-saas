-- MySQL dump 10.13  Distrib 8.0.21, for macos10.15 (x86_64)
--
-- Host: localhost    Database: lab
-- ------------------------------------------------------
-- Server version	8.0.23
--
-- Table structure for table `publications`
--

# --- !Ups
CREATE TABLE `publication` (
  `id` int NOT NULL AUTO_INCREMENT,
  `author` varchar(50) DEFAULT NULL,
  `orc_id` varchar(250) DEFAULT NULL,
  `editor` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `book_title` varchar(250) DEFAULT NULL,
  `pages` varchar(250) DEFAULT NULL,
  `year` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `journal` varchar(250) DEFAULT NULL,
  `volume` varchar(250) DEFAULT NULL,
  `number` varchar(250) DEFAULT NULL,
  `month` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `ee` varchar(500) DEFAULT NULL,
  `cdrom` varchar(250) DEFAULT NULL,
  `cite` varchar(250) DEFAULT NULL,
  `publisher` varchar(250) DEFAULT NULL,
  `note` varchar(500) DEFAULT NULL,
  `crossref` varchar(250) DEFAULT NULL,
  `isbn` varchar(250) DEFAULT NULL,
  `series` varchar(250) DEFAULT NULL,
  `school` varchar(250) DEFAULT NULL,
  `chapter` varchar(250) DEFAULT NULL,
  `publ_type` varchar(250) DEFAULT NULL,
  `mdate` varchar(250) DEFAULT NULL,
  `reviewid` varchar(250) DEFAULT NULL,
  `rating` varchar(250) DEFAULT NULL,
  `cdate` varchar(250) DEFAULT NULL,
  `dblp` varchar(250) DEFAULT NULL,
  `pub_key` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

# --- !Downs
DROP TABLE IF EXISTS `publication`;