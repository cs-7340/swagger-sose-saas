# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

# An example controller showing a sample home page
GET     /                           controllers.HomeController.index

GET   /papers/:name          controllers.PaperController.getPublicationMetadata(name: String)

GET   /papers/:name/abstract          controllers.PaperController.getAbstract(name: String)

GET   /journals/:name/:year/:issue          controllers.PaperController.getPapersPublishedMetadata(name: String, year:String, issue:String)

GET   /articles/:researcherName/:year/titles          controllers.PaperController.getArticleTitles(researcherName: String, year:String)

GET   /articles/:researcherName/:year          controllers.PaperController.getArticles(researcherName: String, year:String)

GET   /authors/:area/:numberOfPapers          controllers.AuthorController.getAuthorsWithOverNumberOfPapersInArea(area: String, numberOfPapers: Int)

GET   /authors/:area/:numberOfPapers/coauthors          controllers.AuthorController.getCoAuthorOfAuthorsWithOverNumberOfPapersInArea(area: String, numberOfPapers: Int)

GET   /conferences/:conferenceName/:startYear/:stopYear          controllers.ConferenceController.findConferenceYearLocation(conferenceName: String, startYear: String, stopYear: String)

GET   /lda/loaddata          controllers.LdaController.loadDataIntoDB()

GET   /lda/model/train/:retrain          controllers.LdaController.trainModel(retrain: Boolean)

GET   /lda/model/delete          controllers.LdaController.deleteModel()

GET   /lda/categories          controllers.LdaController.getCategories()

GET   /lda/categories/:topic         controllers.LdaController.getPublicationsByCategory(topic: String)

GET   /qos/find          controllers.QosController.findOptimizedServiceComposition()

# Swagger API
GET   /swagger.json                 controllers.ApiHelpController.getResources
GET   /docs/                        controllers.Assets.at(path="/public/swagger-ui",file="index.html")
GET   /docs/*file                   controllers.Assets.at(path="/public/swagger-ui",file)