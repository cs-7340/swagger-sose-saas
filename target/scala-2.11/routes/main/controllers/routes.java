
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/gladysadjei/Documents/Lab-4/ebean-backend/conf/routes
// @DATE:Fri Apr 30 11:08:14 CDT 2021

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReversePaperController PaperController = new controllers.ReversePaperController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAuthorController AuthorController = new controllers.ReverseAuthorController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseConferenceController ConferenceController = new controllers.ReverseConferenceController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseHomeController HomeController = new controllers.ReverseHomeController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseLdaController LdaController = new controllers.ReverseLdaController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseApiHelpController ApiHelpController = new controllers.ReverseApiHelpController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseQosController QosController = new controllers.ReverseQosController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReversePaperController PaperController = new controllers.javascript.ReversePaperController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAuthorController AuthorController = new controllers.javascript.ReverseAuthorController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseConferenceController ConferenceController = new controllers.javascript.ReverseConferenceController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseHomeController HomeController = new controllers.javascript.ReverseHomeController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseLdaController LdaController = new controllers.javascript.ReverseLdaController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseApiHelpController ApiHelpController = new controllers.javascript.ReverseApiHelpController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseQosController QosController = new controllers.javascript.ReverseQosController(RoutesPrefix.byNamePrefix());
  }

}
