
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/gladysadjei/Documents/Lab-4/ebean-backend/conf/routes
// @DATE:Fri Apr 30 11:08:14 CDT 2021

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {
  import ReverseRouteContext.empty

  // @LINE:38
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:38
    def at: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.at",
      """
        function(file1) {
        
          if (file1 == """ + implicitly[JavascriptLiteral[String]].to("index.html") + """) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "docs/"})
          }
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "docs/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file1)})
          }
        
        }
      """
    )
  
  }

  // @LINE:8
  class ReversePaperController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:16
    def getArticles: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PaperController.getArticles",
      """
        function(researcherName0,year1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "articles/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("researcherName", encodeURIComponent(researcherName0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("year", encodeURIComponent(year1))})
        }
      """
    )
  
    // @LINE:10
    def getAbstract: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PaperController.getAbstract",
      """
        function(name0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "papers/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name0)) + "/abstract"})
        }
      """
    )
  
    // @LINE:12
    def getPapersPublishedMetadata: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PaperController.getPapersPublishedMetadata",
      """
        function(name0,year1,issue2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "journals/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("year", encodeURIComponent(year1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("issue", encodeURIComponent(issue2))})
        }
      """
    )
  
    // @LINE:8
    def getPublicationMetadata: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PaperController.getPublicationMetadata",
      """
        function(name0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "papers/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name0))})
        }
      """
    )
  
    // @LINE:14
    def getArticleTitles: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PaperController.getArticleTitles",
      """
        function(researcherName0,year1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "articles/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("researcherName", encodeURIComponent(researcherName0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("year", encodeURIComponent(year1)) + "/titles"})
        }
      """
    )
  
  }

  // @LINE:18
  class ReverseAuthorController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:20
    def getCoAuthorOfAuthorsWithOverNumberOfPapersInArea: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AuthorController.getCoAuthorOfAuthorsWithOverNumberOfPapersInArea",
      """
        function(area0,numberOfPapers1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "authors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("area", encodeURIComponent(area0)) + "/" + (""" + implicitly[PathBindable[Int]].javascriptUnbind + """)("numberOfPapers", numberOfPapers1) + "/coauthors"})
        }
      """
    )
  
    // @LINE:18
    def getAuthorsWithOverNumberOfPapersInArea: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AuthorController.getAuthorsWithOverNumberOfPapersInArea",
      """
        function(area0,numberOfPapers1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "authors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("area", encodeURIComponent(area0)) + "/" + (""" + implicitly[PathBindable[Int]].javascriptUnbind + """)("numberOfPapers", numberOfPapers1)})
        }
      """
    )
  
  }

  // @LINE:22
  class ReverseConferenceController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:22
    def findConferenceYearLocation: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ConferenceController.findConferenceYearLocation",
      """
        function(conferenceName0,startYear1,stopYear2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "conferences/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("conferenceName", encodeURIComponent(conferenceName0)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("startYear", encodeURIComponent(startYear1)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("stopYear", encodeURIComponent(stopYear2))})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }

  // @LINE:24
  class ReverseLdaController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:24
    def loadDataIntoDB: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LdaController.loadDataIntoDB",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "lda/loaddata"})
        }
      """
    )
  
    // @LINE:28
    def deleteModel: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LdaController.deleteModel",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "lda/model/delete"})
        }
      """
    )
  
    // @LINE:32
    def getPublicationsByCategory: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LdaController.getPublicationsByCategory",
      """
        function(topic0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "lda/categories/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("topic", encodeURIComponent(topic0))})
        }
      """
    )
  
    // @LINE:26
    def trainModel: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LdaController.trainModel",
      """
        function(retrain0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "lda/model/train/" + (""" + implicitly[PathBindable[Boolean]].javascriptUnbind + """)("retrain", retrain0)})
        }
      """
    )
  
    // @LINE:30
    def getCategories: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LdaController.getCategories",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "lda/categories"})
        }
      """
    )
  
  }

  // @LINE:37
  class ReverseApiHelpController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:37
    def getResources: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ApiHelpController.getResources",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "swagger.json"})
        }
      """
    )
  
  }

  // @LINE:34
  class ReverseQosController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:34
    def findOptimizedServiceComposition: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.QosController.findOptimizedServiceComposition",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "qos/find"})
        }
      """
    )
  
  }


}
