
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/gladysadjei/Documents/Lab-4/ebean-backend/conf/routes
// @DATE:Fri Apr 30 11:08:14 CDT 2021

import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers {

  // @LINE:38
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:38
    def at(file:String): Call = {
    
      (file: @unchecked) match {
      
        // @LINE:38
        case (file) if file == "index.html" =>
          implicit val _rrc = new ReverseRouteContext(Map(("path", "/public/swagger-ui"), ("file", "index.html")))
          Call("GET", _prefix + { _defaultPrefix } + "docs/")
      
        // @LINE:39
        case (file)  =>
          implicit val _rrc = new ReverseRouteContext(Map(("path", "/public/swagger-ui")))
          Call("GET", _prefix + { _defaultPrefix } + "docs/" + implicitly[PathBindable[String]].unbind("file", file))
      
      }
    
    }
  
  }

  // @LINE:8
  class ReversePaperController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:16
    def getArticles(researcherName:String, year:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "articles/" + implicitly[PathBindable[String]].unbind("researcherName", dynamicString(researcherName)) + "/" + implicitly[PathBindable[String]].unbind("year", dynamicString(year)))
    }
  
    // @LINE:10
    def getAbstract(name:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "papers/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)) + "/abstract")
    }
  
    // @LINE:12
    def getPapersPublishedMetadata(name:String, year:String, issue:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "journals/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)) + "/" + implicitly[PathBindable[String]].unbind("year", dynamicString(year)) + "/" + implicitly[PathBindable[String]].unbind("issue", dynamicString(issue)))
    }
  
    // @LINE:8
    def getPublicationMetadata(name:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "papers/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)))
    }
  
    // @LINE:14
    def getArticleTitles(researcherName:String, year:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "articles/" + implicitly[PathBindable[String]].unbind("researcherName", dynamicString(researcherName)) + "/" + implicitly[PathBindable[String]].unbind("year", dynamicString(year)) + "/titles")
    }
  
  }

  // @LINE:18
  class ReverseAuthorController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:20
    def getCoAuthorOfAuthorsWithOverNumberOfPapersInArea(area:String, numberOfPapers:Int): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "authors/" + implicitly[PathBindable[String]].unbind("area", dynamicString(area)) + "/" + implicitly[PathBindable[Int]].unbind("numberOfPapers", numberOfPapers) + "/coauthors")
    }
  
    // @LINE:18
    def getAuthorsWithOverNumberOfPapersInArea(area:String, numberOfPapers:Int): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "authors/" + implicitly[PathBindable[String]].unbind("area", dynamicString(area)) + "/" + implicitly[PathBindable[Int]].unbind("numberOfPapers", numberOfPapers))
    }
  
  }

  // @LINE:22
  class ReverseConferenceController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:22
    def findConferenceYearLocation(conferenceName:String, startYear:String, stopYear:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "conferences/" + implicitly[PathBindable[String]].unbind("conferenceName", dynamicString(conferenceName)) + "/" + implicitly[PathBindable[String]].unbind("startYear", dynamicString(startYear)) + "/" + implicitly[PathBindable[String]].unbind("stopYear", dynamicString(stopYear)))
    }
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def index(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix)
    }
  
  }

  // @LINE:24
  class ReverseLdaController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:24
    def loadDataIntoDB(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "lda/loaddata")
    }
  
    // @LINE:28
    def deleteModel(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "lda/model/delete")
    }
  
    // @LINE:32
    def getPublicationsByCategory(topic:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "lda/categories/" + implicitly[PathBindable[String]].unbind("topic", dynamicString(topic)))
    }
  
    // @LINE:26
    def trainModel(retrain:Boolean): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "lda/model/train/" + implicitly[PathBindable[Boolean]].unbind("retrain", retrain))
    }
  
    // @LINE:30
    def getCategories(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "lda/categories")
    }
  
  }

  // @LINE:37
  class ReverseApiHelpController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:37
    def getResources(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "swagger.json")
    }
  
  }

  // @LINE:34
  class ReverseQosController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:34
    def findOptimizedServiceComposition(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "qos/find")
    }
  
  }


}
