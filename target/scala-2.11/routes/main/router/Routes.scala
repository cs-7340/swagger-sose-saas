
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/gladysadjei/Documents/Lab-4/ebean-backend/conf/routes
// @DATE:Fri Apr 30 11:08:14 CDT 2021

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  HomeController_0: controllers.HomeController,
  // @LINE:8
  PaperController_4: controllers.PaperController,
  // @LINE:18
  AuthorController_1: controllers.AuthorController,
  // @LINE:22
  ConferenceController_3: controllers.ConferenceController,
  // @LINE:24
  LdaController_5: controllers.LdaController,
  // @LINE:34
  QosController_2: controllers.QosController,
  // @LINE:37
  ApiHelpController_7: controllers.ApiHelpController,
  // @LINE:38
  Assets_6: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    HomeController_0: controllers.HomeController,
    // @LINE:8
    PaperController_4: controllers.PaperController,
    // @LINE:18
    AuthorController_1: controllers.AuthorController,
    // @LINE:22
    ConferenceController_3: controllers.ConferenceController,
    // @LINE:24
    LdaController_5: controllers.LdaController,
    // @LINE:34
    QosController_2: controllers.QosController,
    // @LINE:37
    ApiHelpController_7: controllers.ApiHelpController,
    // @LINE:38
    Assets_6: controllers.Assets
  ) = this(errorHandler, HomeController_0, PaperController_4, AuthorController_1, ConferenceController_3, LdaController_5, QosController_2, ApiHelpController_7, Assets_6, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_0, PaperController_4, AuthorController_1, ConferenceController_3, LdaController_5, QosController_2, ApiHelpController_7, Assets_6, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """papers/""" + "$" + """name<[^/]+>""", """controllers.PaperController.getPublicationMetadata(name:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """papers/""" + "$" + """name<[^/]+>/abstract""", """controllers.PaperController.getAbstract(name:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """journals/""" + "$" + """name<[^/]+>/""" + "$" + """year<[^/]+>/""" + "$" + """issue<[^/]+>""", """controllers.PaperController.getPapersPublishedMetadata(name:String, year:String, issue:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """articles/""" + "$" + """researcherName<[^/]+>/""" + "$" + """year<[^/]+>/titles""", """controllers.PaperController.getArticleTitles(researcherName:String, year:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """articles/""" + "$" + """researcherName<[^/]+>/""" + "$" + """year<[^/]+>""", """controllers.PaperController.getArticles(researcherName:String, year:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """authors/""" + "$" + """area<[^/]+>/""" + "$" + """numberOfPapers<[^/]+>""", """controllers.AuthorController.getAuthorsWithOverNumberOfPapersInArea(area:String, numberOfPapers:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """authors/""" + "$" + """area<[^/]+>/""" + "$" + """numberOfPapers<[^/]+>/coauthors""", """controllers.AuthorController.getCoAuthorOfAuthorsWithOverNumberOfPapersInArea(area:String, numberOfPapers:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """conferences/""" + "$" + """conferenceName<[^/]+>/""" + "$" + """startYear<[^/]+>/""" + "$" + """stopYear<[^/]+>""", """controllers.ConferenceController.findConferenceYearLocation(conferenceName:String, startYear:String, stopYear:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """lda/loaddata""", """controllers.LdaController.loadDataIntoDB()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """lda/model/train/""" + "$" + """retrain<[^/]+>""", """controllers.LdaController.trainModel(retrain:Boolean)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """lda/model/delete""", """controllers.LdaController.deleteModel()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """lda/categories""", """controllers.LdaController.getCategories()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """lda/categories/""" + "$" + """topic<[^/]+>""", """controllers.LdaController.getPublicationsByCategory(topic:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """qos/find""", """controllers.QosController.findOptimizedServiceComposition()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """swagger.json""", """controllers.ApiHelpController.getResources"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """docs/""", """controllers.Assets.at(path:String = "/public/swagger-ui", file:String = "index.html")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """docs/""" + "$" + """file<.+>""", """controllers.Assets.at(path:String = "/public/swagger-ui", file:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_0.index,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      """ An example controller showing a sample home page""",
      this.prefix + """"""
    )
  )

  // @LINE:8
  private[this] lazy val controllers_PaperController_getPublicationMetadata1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("papers/"), DynamicPart("name", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PaperController_getPublicationMetadata1_invoker = createInvoker(
    PaperController_4.getPublicationMetadata(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PaperController",
      "getPublicationMetadata",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """papers/""" + "$" + """name<[^/]+>"""
    )
  )

  // @LINE:10
  private[this] lazy val controllers_PaperController_getAbstract2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("papers/"), DynamicPart("name", """[^/]+""",true), StaticPart("/abstract")))
  )
  private[this] lazy val controllers_PaperController_getAbstract2_invoker = createInvoker(
    PaperController_4.getAbstract(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PaperController",
      "getAbstract",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """papers/""" + "$" + """name<[^/]+>/abstract"""
    )
  )

  // @LINE:12
  private[this] lazy val controllers_PaperController_getPapersPublishedMetadata3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("journals/"), DynamicPart("name", """[^/]+""",true), StaticPart("/"), DynamicPart("year", """[^/]+""",true), StaticPart("/"), DynamicPart("issue", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PaperController_getPapersPublishedMetadata3_invoker = createInvoker(
    PaperController_4.getPapersPublishedMetadata(fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PaperController",
      "getPapersPublishedMetadata",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """journals/""" + "$" + """name<[^/]+>/""" + "$" + """year<[^/]+>/""" + "$" + """issue<[^/]+>"""
    )
  )

  // @LINE:14
  private[this] lazy val controllers_PaperController_getArticleTitles4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("articles/"), DynamicPart("researcherName", """[^/]+""",true), StaticPart("/"), DynamicPart("year", """[^/]+""",true), StaticPart("/titles")))
  )
  private[this] lazy val controllers_PaperController_getArticleTitles4_invoker = createInvoker(
    PaperController_4.getArticleTitles(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PaperController",
      "getArticleTitles",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """articles/""" + "$" + """researcherName<[^/]+>/""" + "$" + """year<[^/]+>/titles"""
    )
  )

  // @LINE:16
  private[this] lazy val controllers_PaperController_getArticles5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("articles/"), DynamicPart("researcherName", """[^/]+""",true), StaticPart("/"), DynamicPart("year", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PaperController_getArticles5_invoker = createInvoker(
    PaperController_4.getArticles(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PaperController",
      "getArticles",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """articles/""" + "$" + """researcherName<[^/]+>/""" + "$" + """year<[^/]+>"""
    )
  )

  // @LINE:18
  private[this] lazy val controllers_AuthorController_getAuthorsWithOverNumberOfPapersInArea6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("authors/"), DynamicPart("area", """[^/]+""",true), StaticPart("/"), DynamicPart("numberOfPapers", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AuthorController_getAuthorsWithOverNumberOfPapersInArea6_invoker = createInvoker(
    AuthorController_1.getAuthorsWithOverNumberOfPapersInArea(fakeValue[String], fakeValue[Int]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AuthorController",
      "getAuthorsWithOverNumberOfPapersInArea",
      Seq(classOf[String], classOf[Int]),
      "GET",
      """""",
      this.prefix + """authors/""" + "$" + """area<[^/]+>/""" + "$" + """numberOfPapers<[^/]+>"""
    )
  )

  // @LINE:20
  private[this] lazy val controllers_AuthorController_getCoAuthorOfAuthorsWithOverNumberOfPapersInArea7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("authors/"), DynamicPart("area", """[^/]+""",true), StaticPart("/"), DynamicPart("numberOfPapers", """[^/]+""",true), StaticPart("/coauthors")))
  )
  private[this] lazy val controllers_AuthorController_getCoAuthorOfAuthorsWithOverNumberOfPapersInArea7_invoker = createInvoker(
    AuthorController_1.getCoAuthorOfAuthorsWithOverNumberOfPapersInArea(fakeValue[String], fakeValue[Int]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AuthorController",
      "getCoAuthorOfAuthorsWithOverNumberOfPapersInArea",
      Seq(classOf[String], classOf[Int]),
      "GET",
      """""",
      this.prefix + """authors/""" + "$" + """area<[^/]+>/""" + "$" + """numberOfPapers<[^/]+>/coauthors"""
    )
  )

  // @LINE:22
  private[this] lazy val controllers_ConferenceController_findConferenceYearLocation8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("conferences/"), DynamicPart("conferenceName", """[^/]+""",true), StaticPart("/"), DynamicPart("startYear", """[^/]+""",true), StaticPart("/"), DynamicPart("stopYear", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ConferenceController_findConferenceYearLocation8_invoker = createInvoker(
    ConferenceController_3.findConferenceYearLocation(fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ConferenceController",
      "findConferenceYearLocation",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """conferences/""" + "$" + """conferenceName<[^/]+>/""" + "$" + """startYear<[^/]+>/""" + "$" + """stopYear<[^/]+>"""
    )
  )

  // @LINE:24
  private[this] lazy val controllers_LdaController_loadDataIntoDB9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("lda/loaddata")))
  )
  private[this] lazy val controllers_LdaController_loadDataIntoDB9_invoker = createInvoker(
    LdaController_5.loadDataIntoDB(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LdaController",
      "loadDataIntoDB",
      Nil,
      "GET",
      """""",
      this.prefix + """lda/loaddata"""
    )
  )

  // @LINE:26
  private[this] lazy val controllers_LdaController_trainModel10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("lda/model/train/"), DynamicPart("retrain", """[^/]+""",true)))
  )
  private[this] lazy val controllers_LdaController_trainModel10_invoker = createInvoker(
    LdaController_5.trainModel(fakeValue[Boolean]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LdaController",
      "trainModel",
      Seq(classOf[Boolean]),
      "GET",
      """""",
      this.prefix + """lda/model/train/""" + "$" + """retrain<[^/]+>"""
    )
  )

  // @LINE:28
  private[this] lazy val controllers_LdaController_deleteModel11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("lda/model/delete")))
  )
  private[this] lazy val controllers_LdaController_deleteModel11_invoker = createInvoker(
    LdaController_5.deleteModel(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LdaController",
      "deleteModel",
      Nil,
      "GET",
      """""",
      this.prefix + """lda/model/delete"""
    )
  )

  // @LINE:30
  private[this] lazy val controllers_LdaController_getCategories12_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("lda/categories")))
  )
  private[this] lazy val controllers_LdaController_getCategories12_invoker = createInvoker(
    LdaController_5.getCategories(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LdaController",
      "getCategories",
      Nil,
      "GET",
      """""",
      this.prefix + """lda/categories"""
    )
  )

  // @LINE:32
  private[this] lazy val controllers_LdaController_getPublicationsByCategory13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("lda/categories/"), DynamicPart("topic", """[^/]+""",true)))
  )
  private[this] lazy val controllers_LdaController_getPublicationsByCategory13_invoker = createInvoker(
    LdaController_5.getPublicationsByCategory(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LdaController",
      "getPublicationsByCategory",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """lda/categories/""" + "$" + """topic<[^/]+>"""
    )
  )

  // @LINE:34
  private[this] lazy val controllers_QosController_findOptimizedServiceComposition14_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("qos/find")))
  )
  private[this] lazy val controllers_QosController_findOptimizedServiceComposition14_invoker = createInvoker(
    QosController_2.findOptimizedServiceComposition(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.QosController",
      "findOptimizedServiceComposition",
      Nil,
      "GET",
      """""",
      this.prefix + """qos/find"""
    )
  )

  // @LINE:37
  private[this] lazy val controllers_ApiHelpController_getResources15_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("swagger.json")))
  )
  private[this] lazy val controllers_ApiHelpController_getResources15_invoker = createInvoker(
    ApiHelpController_7.getResources,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ApiHelpController",
      "getResources",
      Nil,
      "GET",
      """ Swagger API""",
      this.prefix + """swagger.json"""
    )
  )

  // @LINE:38
  private[this] lazy val controllers_Assets_at16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("docs/")))
  )
  private[this] lazy val controllers_Assets_at16_invoker = createInvoker(
    Assets_6.at(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "at",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """docs/"""
    )
  )

  // @LINE:39
  private[this] lazy val controllers_Assets_at17_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("docs/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_at17_invoker = createInvoker(
    Assets_6.at(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "at",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """docs/""" + "$" + """file<.+>"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_HomeController_index0_route(params) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_0.index)
      }
  
    // @LINE:8
    case controllers_PaperController_getPublicationMetadata1_route(params) =>
      call(params.fromPath[String]("name", None)) { (name) =>
        controllers_PaperController_getPublicationMetadata1_invoker.call(PaperController_4.getPublicationMetadata(name))
      }
  
    // @LINE:10
    case controllers_PaperController_getAbstract2_route(params) =>
      call(params.fromPath[String]("name", None)) { (name) =>
        controllers_PaperController_getAbstract2_invoker.call(PaperController_4.getAbstract(name))
      }
  
    // @LINE:12
    case controllers_PaperController_getPapersPublishedMetadata3_route(params) =>
      call(params.fromPath[String]("name", None), params.fromPath[String]("year", None), params.fromPath[String]("issue", None)) { (name, year, issue) =>
        controllers_PaperController_getPapersPublishedMetadata3_invoker.call(PaperController_4.getPapersPublishedMetadata(name, year, issue))
      }
  
    // @LINE:14
    case controllers_PaperController_getArticleTitles4_route(params) =>
      call(params.fromPath[String]("researcherName", None), params.fromPath[String]("year", None)) { (researcherName, year) =>
        controllers_PaperController_getArticleTitles4_invoker.call(PaperController_4.getArticleTitles(researcherName, year))
      }
  
    // @LINE:16
    case controllers_PaperController_getArticles5_route(params) =>
      call(params.fromPath[String]("researcherName", None), params.fromPath[String]("year", None)) { (researcherName, year) =>
        controllers_PaperController_getArticles5_invoker.call(PaperController_4.getArticles(researcherName, year))
      }
  
    // @LINE:18
    case controllers_AuthorController_getAuthorsWithOverNumberOfPapersInArea6_route(params) =>
      call(params.fromPath[String]("area", None), params.fromPath[Int]("numberOfPapers", None)) { (area, numberOfPapers) =>
        controllers_AuthorController_getAuthorsWithOverNumberOfPapersInArea6_invoker.call(AuthorController_1.getAuthorsWithOverNumberOfPapersInArea(area, numberOfPapers))
      }
  
    // @LINE:20
    case controllers_AuthorController_getCoAuthorOfAuthorsWithOverNumberOfPapersInArea7_route(params) =>
      call(params.fromPath[String]("area", None), params.fromPath[Int]("numberOfPapers", None)) { (area, numberOfPapers) =>
        controllers_AuthorController_getCoAuthorOfAuthorsWithOverNumberOfPapersInArea7_invoker.call(AuthorController_1.getCoAuthorOfAuthorsWithOverNumberOfPapersInArea(area, numberOfPapers))
      }
  
    // @LINE:22
    case controllers_ConferenceController_findConferenceYearLocation8_route(params) =>
      call(params.fromPath[String]("conferenceName", None), params.fromPath[String]("startYear", None), params.fromPath[String]("stopYear", None)) { (conferenceName, startYear, stopYear) =>
        controllers_ConferenceController_findConferenceYearLocation8_invoker.call(ConferenceController_3.findConferenceYearLocation(conferenceName, startYear, stopYear))
      }
  
    // @LINE:24
    case controllers_LdaController_loadDataIntoDB9_route(params) =>
      call { 
        controllers_LdaController_loadDataIntoDB9_invoker.call(LdaController_5.loadDataIntoDB())
      }
  
    // @LINE:26
    case controllers_LdaController_trainModel10_route(params) =>
      call(params.fromPath[Boolean]("retrain", None)) { (retrain) =>
        controllers_LdaController_trainModel10_invoker.call(LdaController_5.trainModel(retrain))
      }
  
    // @LINE:28
    case controllers_LdaController_deleteModel11_route(params) =>
      call { 
        controllers_LdaController_deleteModel11_invoker.call(LdaController_5.deleteModel())
      }
  
    // @LINE:30
    case controllers_LdaController_getCategories12_route(params) =>
      call { 
        controllers_LdaController_getCategories12_invoker.call(LdaController_5.getCategories())
      }
  
    // @LINE:32
    case controllers_LdaController_getPublicationsByCategory13_route(params) =>
      call(params.fromPath[String]("topic", None)) { (topic) =>
        controllers_LdaController_getPublicationsByCategory13_invoker.call(LdaController_5.getPublicationsByCategory(topic))
      }
  
    // @LINE:34
    case controllers_QosController_findOptimizedServiceComposition14_route(params) =>
      call { 
        controllers_QosController_findOptimizedServiceComposition14_invoker.call(QosController_2.findOptimizedServiceComposition())
      }
  
    // @LINE:37
    case controllers_ApiHelpController_getResources15_route(params) =>
      call { 
        controllers_ApiHelpController_getResources15_invoker.call(ApiHelpController_7.getResources)
      }
  
    // @LINE:38
    case controllers_Assets_at16_route(params) =>
      call(Param[String]("path", Right("/public/swagger-ui")), Param[String]("file", Right("index.html"))) { (path, file) =>
        controllers_Assets_at16_invoker.call(Assets_6.at(path, file))
      }
  
    // @LINE:39
    case controllers_Assets_at17_route(params) =>
      call(Param[String]("path", Right("/public/swagger-ui")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_at17_invoker.call(Assets_6.at(path, file))
      }
  }
}
