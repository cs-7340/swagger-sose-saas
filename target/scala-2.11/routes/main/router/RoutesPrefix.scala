
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/gladysadjei/Documents/Lab-4/ebean-backend/conf/routes
// @DATE:Fri Apr 30 11:08:14 CDT 2021


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
